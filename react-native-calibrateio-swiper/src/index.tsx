import * as React from "react";
import {
  StyleSheet,
  Animated,
  PanResponder,
  Dimensions,
  View,
  LayoutAnimation,
  UIManager
} from "react-native";

export interface Props {
  children: Array<any>;
  onSwipeLeft?: Function;
  onSwipeRight?: Function;
  onIndexChange?: Function;
}

interface State {
  index: number;
}

interface Style {
  cardStyle: any;
  container: any;
}

const SCREEN_WIDTH = Dimensions.get("window").width;
const SWIPE_THRESHOLD = SCREEN_WIDTH * 0.4;

const styles = StyleSheet.create<Style>({
  cardStyle: {
    position: "absolute",
    flexDirection: "row",
    height: "100%",
    width: "100%",
    flex: 1
  },
  container: {
    flex: 1,
    flexDirection: "column"
  }
});

export default class Swiper extends React.Component<Props, State> {
  _panResponder: any;
  _position: any;
  _animation: any;

  constructor(props) {
    super(props);

    this._position = new Animated.ValueXY();
    this._animation = this._position.x.interpolate({
      inputRange: [2 * -SCREEN_WIDTH, 0, 2 * SCREEN_WIDTH],
      outputRange: ["-59deg", "0deg", "59deg"]
    });
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderMove: (evt, gestureState) =>
        this.handleOnPanResponderMove(evt, gestureState),
      onPanResponderRelease: (evt, gestureState) =>
        this.handleOnResponderRelease(evt, gestureState)
    });

    this.state = { index: 0 };
  }

  handleOnPanResponderMove(event, gestureState) {
    this._position.setValue({
      x: gestureState.dx,
      y: gestureState.dy
    });
  }

  handleOnResponderRelease(event, gestureState) {
    let toValue = {
      x: 0,
      y: 0
    };
    let method = "timing";
    let duration = 750;
    let dir;

    if (gestureState.dx > SWIPE_THRESHOLD) {
      toValue = {
        x: SCREEN_WIDTH * 2,
        y: -SCREEN_WIDTH
      };
      dir = "R";
    } else if (gestureState.dx < -SWIPE_THRESHOLD) {
      toValue = {
        x: -SCREEN_WIDTH * 2,
        y: -SCREEN_WIDTH
      };
      dir = "L";
    } else {
      method = "spring";
      duration: 100;
    }

    Animated[method](this._position, {
      toValue,
      duration
    }).start(() => method != "spring" && this.handleOnResponderDone(dir));
  }

  handleOnChangeIndex() {
    const { onIndexChange } = this.props;
    if (typeof onIndexChange === "function") onIndexChange(this.state.index);
  }

  jumpLeft() {
    if (this.state.index <= 1) {
      return 1;
    }
    this.setState({ index: this.state.index - 1 }, () => {
      this.handleOnChangeIndex();
    });
  }

  jumpRight() {
    if (this.state.index == this.props.children.length) {
      return;
    }
    this.setState({ index: this.state.index + 1 }, () => {
      this.handleOnChangeIndex();
    });
  }

  handleOnResponderDone(direction) {
    const { onSwipeRight, onSwipeLeft, children } = this.props;
    this._position.setValue({ x: 0, y: 0 });
    this.setState({ index: this.state.index + 1 }, () => {
      this.handleOnChangeIndex();
      if (typeof onSwipeRight === "function" && direction === "R")
        onSwipeRight(this.state.index);
      if (typeof onSwipeLeft === "function" && direction === "L")
        onSwipeLeft(this.state.index);
    });
  }

  renderCards() {
    const { children } = this.props;

    return children
      .map((item, i) => {
        if (i < this.state.index) {
          return null;
        } else if (i === this.state.index) {
          return (
            <Animated.View
              key={i}
              style={[
                {
                  ...this._position.getLayout(),
                  transform: [{ rotate: this._animation }]
                },
                styles.cardStyle
              ]}
              {...this._panResponder.panHandlers}
            >
              {item}
            </Animated.View>
          );
        }

        return (
          <Animated.View key={i} style={styles.cardStyle}>
            {item}
          </Animated.View>
        );
      })
      .reverse();
  }

  componentWillUpdate() {
    UIManager.setLayoutAnimationEnabledExperimental &&
      UIManager.setLayoutAnimationEnabledExperimental(true);
    LayoutAnimation.spring();
  }

  render() {
    return <View style={styles.container}>{this.renderCards()}</View>;
  }
}
