"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var SCREEN_WIDTH = react_native_1.Dimensions.get("window").width;
var SWIPE_THRESHOLD = SCREEN_WIDTH * 0.4;
var styles = react_native_1.StyleSheet.create({
    cardStyle: {
        position: "absolute",
        flexDirection: "row",
        height: "100%",
        width: "100%",
        flex: 1
    },
    container: {
        flex: 1,
        flexDirection: "column"
    }
});
var Swiper = /** @class */ (function (_super) {
    __extends(Swiper, _super);
    function Swiper(props) {
        var _this = _super.call(this, props) || this;
        _this._position = new react_native_1.Animated.ValueXY();
        _this._animation = _this._position.x.interpolate({
            inputRange: [2 * -SCREEN_WIDTH, 0, 2 * SCREEN_WIDTH],
            outputRange: ["-59deg", "0deg", "59deg"]
        });
        _this._panResponder = react_native_1.PanResponder.create({
            onStartShouldSetPanResponder: function (evt, gestureState) { return true; },
            onStartShouldSetPanResponderCapture: function (evt, gestureState) { return true; },
            onMoveShouldSetPanResponder: function (evt, gestureState) { return true; },
            onMoveShouldSetPanResponderCapture: function (evt, gestureState) { return true; },
            onPanResponderMove: function (evt, gestureState) {
                return _this.handleOnPanResponderMove(evt, gestureState);
            },
            onPanResponderRelease: function (evt, gestureState) {
                return _this.handleOnResponderRelease(evt, gestureState);
            }
        });
        _this.state = { index: 0 };
        return _this;
    }
    Swiper.prototype.handleOnPanResponderMove = function (event, gestureState) {
        this._position.setValue({
            x: gestureState.dx,
            y: gestureState.dy
        });
    };
    Swiper.prototype.handleOnResponderRelease = function (event, gestureState) {
        var _this = this;
        var toValue = {
            x: 0,
            y: 0
        };
        var method = "timing";
        var duration = 750;
        var dir;
        if (gestureState.dx > SWIPE_THRESHOLD) {
            toValue = {
                x: SCREEN_WIDTH * 2,
                y: -SCREEN_WIDTH
            };
            dir = "R";
        }
        else if (gestureState.dx < -SWIPE_THRESHOLD) {
            toValue = {
                x: -SCREEN_WIDTH * 2,
                y: -SCREEN_WIDTH
            };
            dir = "L";
        }
        else {
            method = "spring";
            duration: 100;
        }
        react_native_1.Animated[method](this._position, {
            toValue: toValue,
            duration: duration
        }).start(function () { return method != "spring" && _this.handleOnResponderDone(dir); });
    };
    Swiper.prototype.handleOnResponderDone = function (direction) {
        var _a = this.props, onSwipeRight = _a.onSwipeRight, onSwipeLeft = _a.onSwipeLeft, children = _a.children;
        this._position.setValue({ x: 0, y: 0 });
        this.setState({ index: this.state.index + 1 });
        if (typeof onSwipeRight === "function" && direction === "R")
            onSwipeRight();
        if (typeof onSwipeLeft === "function" && direction === "L")
            onSwipeLeft();
    };
    Swiper.prototype.renderCards = function () {
        var _this = this;
        var children = this.props.children;
        return children
            .map(function (item, i) {
            if (i < _this.state.index) {
                return null;
            }
            else if (i === _this.state.index) {
                return (React.createElement(react_native_1.Animated.View, __assign({ key: i, style: [
                        __assign({}, _this._position.getLayout(), { transform: [{ rotate: _this._animation }] }),
                        styles.cardStyle
                    ] }, _this._panResponder.panHandlers), item));
            }
            return (React.createElement(react_native_1.Animated.View, { key: i, style: styles.cardStyle }, item));
        })
            .reverse();
    };
    Swiper.prototype.componentWillUpdate = function () {
        react_native_1.UIManager.setLayoutAnimationEnabledExperimental &&
            react_native_1.UIManager.setLayoutAnimationEnabledExperimental(true);
        react_native_1.LayoutAnimation.spring();
    };
    Swiper.prototype.render = function () {
        return React.createElement(react_native_1.View, { style: styles.container }, this.renderCards());
    };
    return Swiper;
}(React.Component));
exports.default = Swiper;
//# sourceMappingURL=index.js.map