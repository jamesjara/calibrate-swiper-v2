/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity
} from "react-native";

import Icon from "react-native-vector-icons/FontAwesome";
import Swiper from "react-native-calibrateio-swiper";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

type Props = {};
type State = {
  index: string,
  log: string
};
export default class App extends Component<Props, State> {
  state = {
    index: 0,
    log: ""
  };

  getCard({ key, data }) {
    return (
      <View key style={styles.card}>
        <View style={{ height: 100, flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <View style={styles.badge}>
              <Text style={styles.badgeText}>NOW HIRING</Text>
            </View>
          </View>
          <View style={{ flex: 1 }} />
        </View>

        <View style={{ height: 100, flexDirection: "row" }}>
          <View style={{ flex: 1 }} />
          <View style={{ width: 160, height: 50 }}>
            <Image
              style={styles.cardImage}
              source={{
                uri:
                  "https://www.underconsideration.com/brandnew/archives/spot_hero_logo.png"
              }}
            />
          </View>
          <View style={{ flex: 1 }} />
        </View>

        <Text style={styles.headline}>
          #{key} {data.headline}
        </Text>
        <Text style={styles.subtitle}>{data.subtitle}</Text>

        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}> Get Started </Text>
        </TouchableOpacity>
      </View>
    );
  }

  _onPressLeft = () => {
    this._swiper.jumpLeft();
  };

  _onPressRight = () => {
    this._swiper.jumpRight();
  };

  render() {
    const data = Array(6);
    const total = data.length;

    const headlines = [
      "Spot Hero is looking for a Senior UX Designer",
      "Spot Hero is looking for a Senior Developer",
      "Spot Hero is looking for a Seniori Designer",
      "Spot Hero is looking for a Designer",
      "Spot Hero is looking for a CTO",
      "Spot Hero is looking for Hackers"
    ];

    return (
      <View style={{ padding: 40, flex: 1, backgroundColor: "#003E99" }}>
        <Swiper
          ref={swiper => (this._swiper = swiper)}
          onIndexChange={index => {
            this.setState({ index });
          }}
          onSwipeLeft={() => this.setState({ log: "swipe-left" })}
          onSwipeRight={() => this.setState({ log: "swipe-rigth" })}
        >
          {data.fill(1).map((item, i) => {
            return this.getCard({
              key: i,
              data: {
                img:
                  "https://galoremag.com/wp-content/uploads/dump/maggie-lindemann-pretty-girl-galore-mag-nature.jpg",
                headline: headlines[i],
                subtitle:
                  "SpotHero is seeking a Product Design Intern to make impactful contributions to our Product Team."
              }
            });
          })}
        </Swiper>

        <View>
          <Text style={styles.instructions}>{this.state.log}</Text>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "row"
            }}
          >
            <View>
              <TouchableOpacity onPress={this._onPressLeft}>
                <Icon name="arrow-left" size={20} color="#FFFFFF" />
              </TouchableOpacity>
            </View>
            <View style={{ width: 100 }}>
              <Text style={{ textAlign: "center", color: "#FFFFFF" }}>
                {this.state.index}/{total}
              </Text>
            </View>
            <View>
              <TouchableOpacity onPress={this._onPressRight}>
                <Icon name="arrow-right" size={20} color="#FFFFFF" />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  card: {
    borderRadius: 10,
    flex: 1,
    backgroundColor: "white"
    /*  shadowOpacity: 0.75,
              shadowRadius: 5,
              shadowColor: 'gray',
              shadowOffset: { height: 0, width: 0 },
	      */
  },
  cardTitle: {
    position: "absolute",
    zIndex: 1,
    fontSize: 30,
    margin: 10,
    color: "white",
    shadowOpacity: 0.75,
    shadowRadius: 5,
    shadowColor: "white",
    shadowOffset: { height: 0, width: 0 }
  },
  cardImage: {
    zIndex: 0,
    borderRadius: 5,
    width: 160,
    height: 100
  },
  headline: {
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    marginTop: 30,
    marginRight: 20,
    marginLeft: 20
  },
  subtitle: {
    textAlign: "center",
    color: "gray",
    fontSize: 13,
    marginTop: 20,
    marginRight: 30,
    marginLeft: 30
  },
  button: {
    position: "absolute",
    bottom: 0,
    width: 230,
    margin: 40,
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 30,
    marginRight: 30,
    backgroundColor: "white",
    borderRadius: 30,
    borderWidth: 2,
    borderColor: "#96A9B9"
  },

  buttonText: {
    color: "#96A9B9",
    textAlign: "center"
  },
  badge: {
    backgroundColor: "#ADC2D3",

    borderRadius: 10,
    borderWidth: 3,
    width: 80,
    marginTop: 17,
    marginLeft: 15,
    borderColor: "#ADC2D3"
  },
  badgeText: {
    fontSize: 11,
    color: "white"
  }
});
